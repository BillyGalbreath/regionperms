package net.pl3x.bukkit.regionperms.listener;

import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.pl3x.bukkit.regionperms.Logger;
import net.pl3x.bukkit.regionperms.configuration.Lang;
import net.pl3x.bukkit.regionperms.flag.GivePermEnterFlag;
import net.pl3x.bukkit.regionperms.flag.GivePermExitFlag;
import net.pl3x.bukkit.regionperms.flag.RemovePermEnterFlag;
import net.pl3x.bukkit.regionperms.flag.RemovePermExitFlag;
import net.pl3x.bukkit.regionperms.hook.VaultHook;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        checkFlags(event.getPlayer(), event.getTo(), event.getFrom());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        checkFlags(event.getPlayer(), event.getTo(), event.getFrom());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // find regions
        ApplicableRegionSet toRegions = getApplicableRegions(player.getLocation());
        if (toRegions == null) {
            return; // not in any regions
        }

        // give and remove perms for entering regions
        enteringRegions(player, toRegions.getRegions());
    }

    private void checkFlags(Player player, Location to, Location from) {
        if ((to == null || from == null) ||
                from.getWorld().getName().equals(to.getWorld().getName()) &&
                        from.getBlockX() == to.getBlockX() &&
                        from.getBlockY() == to.getBlockY() &&
                        from.getBlockZ() == to.getBlockZ()) {
            return; // did not move full block. ignore
        }

        // the region sets
        final ApplicableRegionSet fromRegions = getApplicableRegions(from);
        final ApplicableRegionSet toRegions = getApplicableRegions(to);

        if (fromRegions == null && toRegions == null) {
            return; // no regions at all. ignore
        }

        // figure out which regions we're entering and exiting
        Set<ProtectedRegion> enteringRegions = new HashSet<>();
        Set<ProtectedRegion> exitingRegions = new HashSet<>();
        if (toRegions != null) {
            enteringRegions.addAll(toRegions.getRegions().stream()
                    .filter(region -> fromRegions == null || !fromRegions.getRegions().contains(region))
                    .collect(Collectors.toList()));
        }
        if (fromRegions != null) {
            exitingRegions.addAll(fromRegions.getRegions().stream()
                    .filter(region -> toRegions == null || !toRegions.getRegions().contains(region))
                    .collect(Collectors.toList()));
        }

        // give and remove perms for entering regions
        enteringRegions(player, enteringRegions);

        // remove and give perms for exiting regions
        exitingRegions(player, exitingRegions);
    }

    private void enteringRegions(Player player, Set<ProtectedRegion> enteringRegions) {
        for (ProtectedRegion region : enteringRegions) {
            String removePerm = RemovePermEnterFlag.getFlag(region);
            if (removePerm != null && !removePerm.isEmpty() && player.hasPermission(removePerm)) {
                Logger.debug("Enter: Removing permission from " + player.getName() + ": " + removePerm);
                VaultHook.removePermission(player, removePerm);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Lang.REMOVE_PERM_ENTER
                                .replace("{player}", player.getName())
                                .replace("{node}", removePerm)
                                .replace("{region}", region.getId())));
            }
            String givePerm = GivePermEnterFlag.getFlag(region);
            if (givePerm != null && !givePerm.isEmpty() && !player.hasPermission(givePerm)) {
                Logger.debug("Enter: Giving permission to " + player.getName() + ": " + givePerm);
                VaultHook.addPermission(player, givePerm);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Lang.GIVE_PERM_ENTER
                                .replace("{player}", player.getName())
                                .replace("{node}", givePerm)
                                .replace("{region}", region.getId())));
            }
        }
    }

    private void exitingRegions(Player player, Set<ProtectedRegion> exitingRegions) {
        for (ProtectedRegion region : exitingRegions) {
            String removePerm = RemovePermExitFlag.getFlag(region);
            if (removePerm != null && !removePerm.isEmpty() && player.hasPermission(removePerm)) {
                Logger.debug("Exit: Removing permission from " + player.getName() + ": " + removePerm);
                VaultHook.removePermission(player, removePerm);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Lang.REMOVE_PERM_EXIT
                                .replace("{player}", player.getName())
                                .replace("{node}", removePerm)
                                .replace("{region}", region.getId())));
            }
            String givePerm = GivePermExitFlag.getFlag(region);
            if (givePerm != null && !givePerm.isEmpty() && !player.hasPermission(givePerm)) {
                Logger.debug("Exit: Giving permission to " + player.getName() + ": " + givePerm);
                VaultHook.addPermission(player, givePerm);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Lang.GIVE_PERM_EXIT
                                .replace("{player}", player.getName())
                                .replace("{node}", givePerm)
                                .replace("{region}", region.getId())));
            }
        }
    }

    private ApplicableRegionSet getApplicableRegions(Location location) {
        RegionManager regionManager = WorldGuardPlugin.getPlugin(WorldGuardPlugin.class).getRegionContainer().get(location.getWorld());
        if (regionManager == null) {
            return null;
        }
        return regionManager.getApplicableRegions(BukkitUtil.toVector(location));
    }
}
