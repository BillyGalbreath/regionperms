package net.pl3x.bukkit.regionperms;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import net.pl3x.bukkit.regionperms.configuration.Lang;
import net.pl3x.bukkit.regionperms.flag.GivePermEnterFlag;
import net.pl3x.bukkit.regionperms.flag.GivePermExitFlag;
import net.pl3x.bukkit.regionperms.flag.RemovePermEnterFlag;
import net.pl3x.bukkit.regionperms.flag.RemovePermExitFlag;
import net.pl3x.bukkit.regionperms.hook.VaultHook;
import net.pl3x.bukkit.regionperms.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RegionPerms extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        boolean failedDependencies = false;
        PluginManager pm = Bukkit.getPluginManager();
        if (!pm.isPluginEnabled("WorldGuard")) {
            Logger.error("WorldGuard not installed/found!");
            failedDependencies = true;
        }

        if (!pm.isPluginEnabled("Vault")) {
            Logger.error("Vault not installed/found!");
            failedDependencies = true;
        }

        if (VaultHook.failedSetupPermissions()) {
            Logger.error("Vault compatible Permissions plugin not installed/found!");
            failedDependencies = true;
        }

        if (failedDependencies) {
            Logger.error("Disabling plugin!");
            pm.disablePlugin(this);
            return;
        }

        injectFlags();

        pm.registerEvents(new PlayerListener(), this);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static RegionPerms getPlugin() {
        return RegionPerms.getPlugin(RegionPerms.class);
    }

    private void injectFlags() {
        try {
            Field field = DefaultFlag.class.getDeclaredField("flagsList");
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & 0xFFFFFFEF);
            field.setAccessible(true);
            //noinspection unchecked
            List elements = new ArrayList(Arrays.asList(DefaultFlag.flagsList));
            //noinspection unchecked
            elements.add(GivePermEnterFlag.getFlag());
            //noinspection unchecked
            elements.add(RemovePermEnterFlag.getFlag());
            //noinspection unchecked
            elements.add(GivePermExitFlag.getFlag());
            //noinspection unchecked
            elements.add(RemovePermExitFlag.getFlag());
            Flag[] list = new Flag[elements.size()];
            for (int i = 0; i < elements.size(); i++) {
                list[i] = ((Flag) elements.get(i));
            }
            field.set(null, list);
            for (RegionManager rm : WorldGuardPlugin.getPlugin(WorldGuardPlugin.class).getRegionContainer().getLoaded()) {
                rm.load();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
