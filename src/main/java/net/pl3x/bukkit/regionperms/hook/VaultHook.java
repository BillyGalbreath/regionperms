package net.pl3x.bukkit.regionperms.hook;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHook {
    private static Permission permission = null;

    public static boolean failedSetupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return permission == null;
    }

    public static void addPermission(Player player, String node) {
        permission.playerAdd(player, node);
    }

    public static void removePermission(Player player, String node) {
        permission.playerRemove(player, node);
    }
}
