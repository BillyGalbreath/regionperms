package net.pl3x.bukkit.regionperms.configuration;

import net.pl3x.bukkit.regionperms.RegionPerms;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml");

    private final RegionPerms plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = RegionPerms.getPlugin();
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }
}
