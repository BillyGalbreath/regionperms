package net.pl3x.bukkit.regionperms.flag;


import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class GivePermEnterFlag extends StringFlag {
    private static final GivePermEnterFlag flag = new GivePermEnterFlag();

    private GivePermEnterFlag() {
        super("give-perm-enter");
    }

    public static GivePermEnterFlag getFlag() {
        return flag;
    }

    public static String getFlag(ProtectedRegion region) {
        return region.getFlag(flag);
    }
}
