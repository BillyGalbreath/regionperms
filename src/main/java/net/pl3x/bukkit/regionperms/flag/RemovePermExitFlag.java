package net.pl3x.bukkit.regionperms.flag;


import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RemovePermExitFlag extends StringFlag {
    private static final RemovePermExitFlag flag = new RemovePermExitFlag();

    private RemovePermExitFlag() {
        super("remove-perm-exit");
    }

    public static RemovePermExitFlag getFlag() {
        return flag;
    }

    public static String getFlag(ProtectedRegion region) {
        return region.getFlag(flag);
    }
}
