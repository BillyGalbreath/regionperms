package net.pl3x.bukkit.regionperms.flag;


import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RemovePermEnterFlag extends StringFlag {
    private static final RemovePermEnterFlag flag = new RemovePermEnterFlag();

    private RemovePermEnterFlag() {
        super("remove-perm-enter");
    }

    public static RemovePermEnterFlag getFlag() {
        return flag;
    }

    public static String getFlag(ProtectedRegion region) {
        return region.getFlag(flag);
    }
}
