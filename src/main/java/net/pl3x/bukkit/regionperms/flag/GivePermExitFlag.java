package net.pl3x.bukkit.regionperms.flag;


import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class GivePermExitFlag extends StringFlag {
    private static final GivePermExitFlag flag = new GivePermExitFlag();

    private GivePermExitFlag() {
        super("give-perm-exit");
    }

    public static GivePermExitFlag getFlag() {
        return flag;
    }

    public static String getFlag(ProtectedRegion region) {
        return region.getFlag(flag);
    }
}
